var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('./public/build/')
    .setPublicPath('/build/')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableSassLoader()
    .addStyleEntry('app', './assets/sass/app.scss')
    .enablePostCssLoader()
;

module.exports = Encore.getWebpackConfig();
