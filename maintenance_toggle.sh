#!/usr/bin/env bash

if [ -f public/.htaccess_maintenance ];
then
    mv public/.htaccess public/.htaccess_site
    mv public/.htaccess_maintenance public/.htaccess
else
    mv public/.htaccess public/.htaccess_maintenance
    mv public/.htaccess_site public/.htaccess
fi
