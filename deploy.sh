#!/usr/bin/env bash

apache_user=$1
env=$2
fix_permissions_file=$3
git_stash_name="deployscript"
error=false

while true; do
    read -p "Current user has full permission on the project ? [Y/n]" yn
    yn=${yn:-Yes}
    case ${yn} in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

if [ -z ${apache_user} ];
then
    read -p "Enter apache user ? [www-data]" apache_user
    apache_user=${apache_user:-www-data}
fi

if [ -z ${env} ];
then
    read -p "Enter env ? [DEV/prod]" env
    env=${env:-dev}
fi

if [ -z ${fix_permissions_file} ];
then
    read -p "Fix permissions file (cache, session, etc..) ? [TRUE/false]" fix_permissions_file
    fix_permissions_file=${fix_permissions_file:-"true"}
fi

if [ -z ${apache_user} ] || [ -z ${env} ] || [ -z ${fix_permissions_file} ] ;
then
    exit 0;
fi

git stash save ${git_stash_name}

if [ -f public/.htaccess_maintenance ]; # Check if maintenance mode is not already enabled
then
    sh ./maintenance_toggle.sh
fi

git_stash_state="KO";
[[ "$(git stash list)" =~ ^stash\@\{0\}.+deployscript ]] && git_stash_state="OK"

git pull || { echo -e "
\e[1;41;97m
    ERROR: Git pull failed

\e[0m" ;  error=true; }

if [ ${error} = false ]; then

    if [ ${env} = "prod" ] || [ ${env} = "PROD" ] || [ ${env} = "Prod" ];
    then
        bin/composer install --no-dev --optimize-autoloader
        php bin/console assets:install --env=prod --no-debug
    else
        bin/composer install
        php bin/console assets:install
        yarn install
    fi

    php bin/console doctrine:migration:migrate --no-interaction
    sh ./clear-cache.sh ${fix_permissions_file} ${apache_user}

    ## Fix the directory's rights
    if [ ${fix_permissions_file} = "true" ] || [ ${fix_permissions_file} = "TRUE" ] || [ ${fix_permissions_file} = "True" ];
    then
        for path in app/ var/
        do
            if [ -d "$path" ]; then # Check if path exist
                for type in /logs /sessions /tmp /spool /uploads
                do
                    if [ -d "$path$type" ]; then # Check if folder exist
                        chown -R ${apache_user} "$path$type"
                    fi
                done
            fi
        done
    fi
fi

if [ ${git_stash_state} = "OK" ];
then
    git stash pop
fi

sh ./maintenance_toggle.sh

if [ ${error} = true ]; then
    echo -e "
    \e[1;41;97m

    ERROR : KO
    \e[0m"
    exit 1;

else
    echo -e "\e[1;42;97m

    DEPLOY : OK
    \e[0m"
    exit 1;
fi
