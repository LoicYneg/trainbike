# Template Project

## Instruction

This is a template project, so to start a new project you need :

- clone project
- go to the project folder
- remove .git folder
- in composer.json change information ( name, description, etc.. )
- in package.json change information ( name, description, etc... )
- in .env change information ( APP_INSTANCE_NAME, COMPOSE_PROJECT_NAME ) and in grumphp.yml change template_sf4 value
- in README.md delete instruction section and change information in Readme section

Now your project is ready to start. 


======================================== README ================================

## Install

### Requirements

This projects runs through the following services in production.

- PHP 7.4
- Apache >= 2.4
- Mysql >= 8.0
- Git

In order to run this project, you need to install several tools to reproduce the same environment easily:

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/overview/)

### Installation

- Clone the project repository
- Go to root project with your terminal and run command :

```bash
make fixtures-install=Y install

# Take a cup of coffee... ;)
```

This command will just clone the project repository, setup a whole dev environment through docker machines (one by entry in the `docker-compose.yml` file) and setup database structure with migrations.

You'll certainly be prompted for some consumer key / secret from bitbucket during composer install. Just generate and tape it, it'll not ask again in the future.


### Email

It's deadly simple to test email thanks to the maildev server which is installed as a container.

To access the email console, just open `localhost:1110` in your browser and you'll be able to see sended email.

## Development

### Starting Project

A single command allows you to run the project:

```sh
make start
```

If you run `make status` after that, you'll see the corresponding list of containers that are running (they're prefixed by `template_sf4_`).

App instance runs on 8080 port, you can access it on `http://localhost:8080`.

### Stopping Project

If you need to stop all project containers, just use:

```sh
make stop
```

### Logs

You can access logs from php container with `make logs`. It's just a shortcut to the docker-compose log command.

If you need more logs on other container, you can use either docker-compose or docker command like:

`docker logs <full name or id of your container>` (obtained with `docker status`)

Sometimes, logs are not enought you'll investiguate from into the containers. To do that, just tape `make connect` and you'll be connected into the php container.

### Database access

All databases ports from this project are incrementally mapped to local ports on your machine which allows to access them from a database explorer software (like Dbeaver...).

- 3346 => template_sf4

You can import sql scripts the same way as you makes usually. For exemple:

```
mysql --host=127.0.0.1 --port=3346 --user=root --password=p@ssword template_sf4 < myscript.sql
```

### More 

More command was available, just ```make help ```.