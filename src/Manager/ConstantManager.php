<?php

declare(strict_types=1);

namespace App\Manager;

use App\Contract\Manager\ConstantManagerInterface;
use App\Entity\BikeSession;
use App\Entity\Constant;
use App\Repository\BikeSessionRepository;
use App\Repository\ConstantRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;

final class ConstantManager implements ConstantManagerInterface
{
    /**
     * @var ConstantRepository
     */
    private $constantRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $repo = $entityManager->getRepository(Constant::class);
        if (!$repo instanceof ConstantRepository) {
            throw new InvalidArgumentException(sprintf(
                'The repository class for "%s" must be "%s" and given "%s"! ' .
                'Maybe look the "repositoryClass" declaration on %s ?',
                Constant::class,
                ConstantRepository::class,
                get_class($repo),
                Constant::class
            ));
        }
        $this->constantRepository = $repo;
    }

    /**
     * {@inheritdoc}
     */
    public function createOrUpdate(Constant $constant, bool $flush = true): void
    {
        /** @var int|null $id */
        $id = $constant->getId();
        if ($id === null) {
            $this->entityManager->persist($constant);
        }
        if ($flush === true) {
            $this->entityManager->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Constant $constant, bool $flush = true): void
    {
        $this->entityManager->remove($constant);
        if ($flush === true) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param array $keyValue
     * @return Constant[]|Collection
     */
    public function findBy(array $keyValue): Collection
    {
        return $this->constantRepository->findBy($keyValue);
    }
}
