<?php

namespace App\Manager;

use App\Contract\Manager\BikeSessionManagerInterface;
use App\Dto\BikeSessionDto;
use App\Entity\Athletic;
use App\Entity\BikeSession;
use App\Repository\BikeSessionRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;

class BikeSessionManager implements BikeSessionManagerInterface
{
    /**
     * @var BikeSessionRepository
     */
    private $bikeSessionRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $repo = $entityManager->getRepository(BikeSession::class);
        if (!$repo instanceof BikeSessionRepository) {
            throw new InvalidArgumentException(sprintf(
                'The repository class for "%s" must be "%s" and given "%s"! ' .
                'Maybe look the "repositoryClass" declaration on %s ?',
                BikeSession::class,
                BikeSessionRepository::class,
                get_class($repo),
                BikeSession::class
            ));
        }
        $this->bikeSessionRepository = $repo;
    }

    /**
     * {@inheritdoc}
     */
    public function createOrUpdate(BikeSession $bikeSession, bool $flush = true): void
    {
        /** @var int|null $id */
        $id = $bikeSession->getId();
        if ($id === null) {
            $this->entityManager->persist($bikeSession);
        }
        if ($flush === true) {
            $this->entityManager->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove(BikeSession $bikeSession, bool $flush = true): void
    {
        $this->entityManager->remove($bikeSession);
        if ($flush === true) {
            $this->entityManager->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByAthletic(Athletic $athletic): Collection
    {
        return new ArrayCollection($this->bikeSessionRepository->findBy(array(
            'athletic' => $athletic->getId()
        )));
    }
}
