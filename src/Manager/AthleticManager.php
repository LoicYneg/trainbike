<?php

declare(strict_types=1);

namespace App\Manager;

use App\Contract\Manager\AthleticManagerInterface;
use App\Entity\Athletic;
use App\Enum\UserRoleEnum;
use App\Repository\AthleticRepository;
use App\ViewModel\StatsViewModel;
use Doctrine\ORM\EntityManagerInterface;
use Doudou\BaseBundle\Helper\DateTimeHelper;
use InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AthleticManager
 * @package App\Manager
 */
final class AthleticManager implements AthleticManagerInterface
{
    /**
    * @var AthleticRepository
     */
    private $athleticRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $encoder;

    public function __construct(
        UserPasswordEncoderInterface $encoder,
        EntityManagerInterface $entityManager
    ) {
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
        $repo = $entityManager->getRepository(Athletic::class);
        if (!$repo instanceof AthleticRepository) {
            throw new InvalidArgumentException(sprintf(
                'The repository class for "%s" must be "%s" and given "%s"! ' .
                'Maybe look the "repositoryClass" declaration on %s ?',
                Athletic::class,
                AthleticRepository::class,
                get_class($repo),
                Athletic::class
            ));
        }
        $this->athleticRepository = $repo;
    }

    /**
     * {@inheritdoc}
     */
    public function createOrUpdate(Athletic $athletic, bool $flush = true): void
    {
        /** @var int|null $id */
        $id = $athletic->getId();
        if ($id === null) {
            $athletic = $this->createNewAthletic($athletic);
            $this->entityManager->persist($athletic);
        }
        if ($flush === true) {
            $this->entityManager->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Athletic $athletic, bool $flush = true): void
    {
        $this->entityManager->remove($athletic);
        if ($flush === true) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param array $keyAndValue
     * @return Athletic|null
     */
    public function findOneBy(array $keyAndValue): ?Athletic
    {
        /** @var Athletic $result */
        $result = $this->athleticRepository->findOneBy($keyAndValue);
        return $result;
    }

    /**
     * @param Athletic $athletic
     * @return StatsViewModel
     */
    public function getAthleticStatistics(Athletic $athletic): StatsViewModel
    {
        $bikeSessions = $athletic->getBikeSessions();
        $kms = 0;
        $kjs = 0;
        $totalDuration = 0;
        foreach ($bikeSessions as $bikeSession) {
            $kms += (int) $bikeSession->getDistance();
            $kjs += (int) $bikeSession->getEnergy();
            $totalDuration += DateTimeHelper::diffBetweenDateTimeInMinutes($bikeSession->getDuration());
        }
        return (new StatsViewModel((float) $kms, (int) $kjs, (int) $totalDuration));
    }

    /**
     * @param Athletic $athletic
     * @return Athletic
     * @throws \Exception
     */
    private function createNewAthletic(Athletic $athletic): Athletic
    {
        $user = $athletic->getUser();
        if (null !== $user) {
            $user->setTimes();
            $password = $user->getPassword();
            $encoded = $this->encoder->encodePassword($user, $password);
            $user->setPassword($encoded);
            $roles = $user->getRoles();
            $roles[] = UserRoleEnum::ROLE_USER;
            $user->setRoles($roles);
            $athletic
                ->setUser($user)
                ->setTimes();
            return $athletic;
        }
        throw new UnauthorizedHttpException('You missed something in user creation');
    }
}
