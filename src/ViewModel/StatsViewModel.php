<?php

declare(strict_types=1);

namespace App\ViewModel;

use DateTimeImmutable;

/**
 * Class StatsViewModel
 * @package App\ViewModel
 */
class StatsViewModel
{

    /**
     * @var float $km
     */
    private $kms;

    /**
     * @var int $kjs;
     */
    private $kjs;

    /**
     * @var int $time
     */
    private $time;

    /**
     * StatsViewModel constructor.
     * @param float $kms
     * @param int $kjs
     * @param int $time
     */
    public function __construct(
        float $kms,
        int $kjs,
        int $time
    ) {
        $this->kms = $kms;
        $this->kjs = $kjs;
        $this->time = $time;
    }

    /**
     * @return float
     */
    public function getKms(): float
    {
        return $this->kms;
    }

    /**
     * @param float $kms
     */
    public function setKms(float $kms): void
    {
        $this->kms = $kms;
    }

    /**
     * @return int
     */
    public function getKjs(): int
    {
        return $this->kjs;
    }

    /**
     * @param int $kjs
     */
    public function setKjs(int $kjs): void
    {
        $this->kjs = $kjs;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     */
    public function setTime(int $time): void
    {
        $this->time = $time;
    }
}