<?php

namespace App\Entity;

use App\Enum\ConstantEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AthleticRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Athletic extends AbstractPerson
{

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="athletic", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $pseudo;

    /**
     * @var Constant[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Constant", mappedBy="athletic", orphanRemoval=true)
     */
    private $vitals;

    /**
     * @var BikeSession[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\BikeSession", mappedBy="athletic", orphanRemoval=true)
     * @OrderBy({"createdAt" = "ASC"})
     */
    private $bikeSessions;

    public function __construct()
    {
        parent::__construct();
        $this->vitals = new ArrayCollection();
        $this->bikeSessions = new ArrayCollection();
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    /**
     * @param string|null $pseudo
     * @return $this
     */
    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * @return Collection|Constant[]
     */
    public function getVitals(): Collection
    {
        return $this->vitals;
    }

    public function addVital(Constant $vital): self
    {
        if (!$this->vitals->contains($vital)) {
            $this->vitals[] = $vital;
            $vital->setAthletic($this);
        }

        return $this;
    }

    public function removeVital(Constant $vital): self
    {
        if ($this->vitals->contains($vital)) {
            $this->vitals->removeElement($vital);
            // set the owning side to null (unless already changed)
            if ($vital->getAthletic() === $this) {
                $vital->setAthletic(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BikeSession[]
     */
    public function getBikeSessions(): Collection
    {
        return $this->bikeSessions;
    }

    public function addBikeSession(BikeSession $bikeSession): self
    {
        if (!$this->bikeSessions->contains($bikeSession)) {
            $this->bikeSessions[] = $bikeSession;
            $bikeSession->setAthletic($this);
        }

        return $this;
    }

    public function removeBikeSession(BikeSession $bikeSession): self
    {
        if ($this->bikeSessions->contains($bikeSession)) {
            $this->bikeSessions->removeElement($bikeSession);
            // set the owning side to null (unless already changed)
            if ($bikeSession->getAthletic() === $this) {
                $bikeSession->setAthletic(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Constant[]
     */
    public function getWeightEvolution(): Collection
    {
        /** @var Constant $vitals */
        $vitals = $this->getVitals();
        $weights = new ArrayCollection();
        foreach ($vitals as $vital) {
            if ($vital->getType() === ConstantEnum::WEIGHT()) {
                $weights->add($vital);
            }
        }
        return $weights;
    }
}
