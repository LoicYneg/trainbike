<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AbstactEntity
 * @package App\Entity
 */
abstract class AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var UuidInterface
     *
     * @ORM\Column(type="uuid", name="`uuid`", unique=true)
     *
     * @Assert\Uuid()
     *
     * @Groups({"READ"})
     */
    protected $uuid;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(type="datetimetz_immutable")
     *
     * @Groups({"READ"})
     */
    protected $createdAt;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(type="datetimetz_immutable")
     *
     * @Groups({"READ"})
     */
    protected $updatedAt;

    /**
     * AbstractEntity constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->generateUuid();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return UuidInterface
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @throws Exception
     * @return void
     */
    protected function generateUuid()
    {
        /** @var UuidInterface|null $uuid */
        $uuid = $this->uuid;
        if ($uuid === null) {
            $this->uuid = Uuid::uuid4();
        }
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeImmutable $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTimeImmutable $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeImmutable $updatedAt
     * @return $this
     */
    public function setUpdatedAt(DateTimeImmutable $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @return void
     * @throws Exception
     */
    public function setTimes()
    {
        /** @var DateTimeImmutable|null $created */
        $created = $this->getCreatedAt();
        if ($created === null) {
            $this->setCreatedAt(new DateTimeImmutable());
        }
        $this->setUpdatedAt(new DateTimeImmutable());
    }
}