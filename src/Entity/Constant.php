<?php

namespace App\Entity;

use App\Enum\ConstantEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConstantRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Constant extends AbstractEntity
{
    /**
     * @var ConstantEnum $type
     * @ORM\Column(type="enum_constant_type", nullable=false)
     */
    private $type;

    /**
     * @var float $value
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=false)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Athletic", inversedBy="vitals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $athletic;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BikeSession", inversedBy="vitals")
     */
    private $bikeSession;

    /**
     * @return ConstantEnum $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param ConstantEnum $type
     */
    public function setType(ConstantEnum $type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value)
    {
        $this->value = $value;
    }

    /**
     * @return Athletic|null
     */
    public function getAthletic(): ?Athletic
    {
        return $this->athletic;
    }

    public function setAthletic(Athletic $athletic)
    {
        $this->athletic = $athletic;

        return $this;
    }

    /**
     * @return BikeSession|null
     */
    public function getBikeSession(): ?BikeSession
    {
        return $this->bikeSession;
    }

    /**
     * @param BikeSession|null $bikeSession
     * @return $this
     */
    public function setBikeSession(?BikeSession $bikeSession): self
    {
        $this->bikeSession = $bikeSession;

        return $this;
    }
}
