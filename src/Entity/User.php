<?php

namespace App\Entity;

use App\Enum\UserRoleEnum;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ReflectionException;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 *
 * @UniqueEntity(fields={"email"})
 */
class User extends AbstractEntity implements UserInterface
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isEmailConfirmed;

    /**
     * @var DateTimeImmutable|null
     *
     * @ORM\Column(type="datetimetz_immutable", nullable=true)
     */
    private $lastLogin;

    /**
     * @var array|string[]
     *
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @var Athletic
     * @ORM\OneToOne(targetEntity="App\Entity\Athletic", mappedBy="user", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    private $athletic;

    public function __construct()
    {
        parent::__construct();
        $this->roles = array();
        $this->isEmailConfirmed = false;
        $this->isEnabled = false;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @param bool $isEnabled
     * @return User
     */
    public function setIsEnabled(bool $isEnabled)
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed()
    {
        return $this->isEmailConfirmed;
    }

    /**
     * @param bool $isEmailConfirmed
     * @return User
     */
    public function setIsEmailConfirmed(bool $isEmailConfirmed)
    {
        $this->isEmailConfirmed = $isEmailConfirmed;
        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @param DateTimeImmutable|null $lastLogin
     * @return User
     */
    public function setLastLogin(?DateTimeImmutable $lastLogin)
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return Collection|UserRoleEnum[]
     * @throws ReflectionException
     */
    public function getEnumRoles()
    {
        $roles = new ArrayCollection();
        foreach ($this->roles as $role) {
            $roles->add(UserRoleEnum::byValue($role));
        }
        return $roles;
    }

    /**
     * @param UserRoleEnum $role
     * @return $this
     */
    public function addRole(UserRoleEnum $role): self
    {
        if (!isset($this->roles, $role)) {
            $this->roles[] = $role;
        }
        return $this;
    }


    /**
     * @param array|string[] $roles
     * @return User
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @param Collection|UserRoleEnum[] $roles
     * @return User
     */
    public function setEnumRoles(Collection $roles)
    {
        $r = array();
        foreach ($roles as $role) {
            $r[] = $role->getValue();
        }
        $this->roles = $r
        ;
        return $this;
    }

    /**
     * @return Athletic
     */
    public function getAthletic(): Athletic
    {
        return $this->athletic;
    }

    /**
     * @param Athletic $athletic
     * @return $this
     */
    public function setAthletic(Athletic $athletic): self
    {
        $this->athletic = $athletic;

        // set the owning side of the relation if necessary
        if ($athletic->getUser() !== $this) {
            $athletic->setUser($this);
        }

        return $this;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function getUsername()
    {
        // TODO: Implement getUsername() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
