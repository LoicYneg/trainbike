<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BikeSessionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class BikeSession extends AbstractEntity
{

    /**
     * @var float;
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $distance;

    /**
     * @var DateTimeImmutable;
     *
     * @ORM\Column(type="datetimetz")
     */
    private $duration;

    /**
     * @var int;
     * @ORM\Column(type="integer", nullable=true)
     */
    private $energy;

    /**
     * @var Constant[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Constant", mappedBy="bikeSession", cascade={"persist"})
     */
    private $vitals;

    /**
     * @var Athletic
     * @ORM\ManyToOne(targetEntity="App\Entity\Athletic", inversedBy="bikeSessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $athletic;

    /**
     * BikeSession constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->vitals = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getDistance(): ?int
    {
        return $this->distance;
    }

    /**
     * @param int|null $distance
     * @return $this
     */
    public function setDistance(?int $distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param DateTimeImmutable $duration
     * @return $this
     */
    public function setDuration(DateTimeImmutable $duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEnergy(): ?int
    {
        return $this->energy;
    }

    /**
     * @param int|null $energy
     * @return $this
     */
    public function setEnergy(?int $energy): self
    {
        $this->energy = $energy;

        return $this;
    }

    /**
     * @return Collection|Constant[]
     */
    public function getVitals(): Collection
    {
        return $this->vitals;
    }

    /**
     * @param Constant $vital
     * @return $this
     */
    public function addVital(Constant $vital): self
    {
        if (!$this->vitals->contains($vital)) {
            $this->vitals[] = $vital;
            $vital->setBikeSession($this);
        }

        return $this;
    }

    /**
     * @param Constant $vital
     * @return $this
     */
    public function removeVital(Constant $vital): self
    {
        if ($this->vitals->contains($vital)) {
            $this->vitals->removeElement($vital);
            // set the owning side to null (unless already changed)
            if ($vital->getBikeSession() === $this) {
                $vital->setBikeSession(null);
            }
        }

        return $this;
    }

    public function getAthletic(): ?Athletic
    {
        return $this->athletic;
    }

    public function setAthletic(?Athletic $athletic): self
    {
        $this->athletic = $athletic;

        return $this;
    }
}
