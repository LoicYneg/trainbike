<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\BikeSession;
use App\Entity\Constant;
use App\Form\Field\ConstantEnumFieldType;
use Doudou\BaseBundle\Form\Field\DateTimeImmutableField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

/**
 * Class BikeSessionType
 * @package App\Form
 */
final class ConstantType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $linked = $options['linked_to_bike_session'];
        $builder
            ->add('type', ConstantEnumFieldType::class, array(
                'required' => true
            ))
            ->add('value', NumberType::class, array(
                'required' => true
            ))
            ->add('bikeSession', Select2EntityType::class, array(
                'multiple' => false,
                'class' => BikeSession::class,
                'remote_route' => 'bike_session_search',
                'primary_key' => 'id',
                'text_property' => 'createdAt',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'delay' => 250,
                'placeholder' => 'Select a Bike Session',
            ))
        ;
        if ($linked === false) {
            $builder->add('createdAt', DateTimeImmutableField::class)
                ->add('submit', SubmitType::class);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Constant::class,
            'linked_to_bike_session' => false
        ));
    }
}
