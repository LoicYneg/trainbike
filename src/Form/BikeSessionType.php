<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\BikeSession;
use Doudou\BaseBundle\Form\Field\DateTimeImmutableField;
use Doudou\BaseBundle\Form\Field\TimeImmutableField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BikeSessionType
 * @package App\Form
 */
final class BikeSessionType extends AbstractType
{
    /**
     * https://symfony.com/doc/current/reference/forms/types/collection.html
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('distance', NumberType::class, array(
                'required' => true
            ))
            ->add('duration', TimeImmutableField::class, array(
                'required' => true
            ))
            ->add('energy', NumberType::class)
            ->add('createdAt', DateTimeImmutableField::class)
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => BikeSession::class
        ));
    }
}