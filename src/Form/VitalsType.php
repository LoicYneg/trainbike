<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Constant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BikeSessionType
 * @package App\Form
 */
final class VitalsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('puls', NumberType::class, array(
                'label' => 'Pulsations'
            ))
            ->add('weight', NumberType::class, array(
                'label' => 'Poids'
            ))
            ->add('glycemia', NumberType::class, array(
                'label' => 'Glycémie'
            ))
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => Constant::class
        ));
    }
}