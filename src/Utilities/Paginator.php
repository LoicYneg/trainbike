<?php

namespace App\Utilities;

use App\Contract\Manager\PaginatorInterface;
use Pagerfanta\Pagerfanta;

class Paginator extends PagerFanta implements PaginatorInterface
{
}