<?php

declare(strict_types=1);

namespace App\Contract\Manager;

use App\Entity\Constant;
use Doctrine\Common\Collections\Collection;

interface ConstantManagerInterface
{
    /**
     * @param Constant$constant
     * @param bool $flush
     */
    public function createOrUpdate(Constant $constant, bool $flush = true): void;

    /**
     * @param Constant $constant
     * @param bool $flush
     * @return void
     */
    public function remove(Constant $constant, bool $flush = true): void;

    /**
     * @param array $keyValue
     * @return Constant[]|Collection
     */
    public function findBy(array $keyValue): Collection;
}