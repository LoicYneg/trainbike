<?php

declare(strict_types=1);

namespace App\Contract\Manager;

use App\Dto\BikeSessionDto;
use App\Entity\Athletic;
use App\Entity\BikeSession;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

interface BikeSessionManagerInterface
{

    /**
     * @param BikeSession $bikeSession
     * @param bool $flush
     */
    public function createOrUpdate(BikeSession $bikeSession, bool $flush = true): void;

    /**
     * @param BikeSession $bikeSession
     * @param bool $flush
     */
    public function remove(BikeSession $bikeSession, bool $flush = true): void;

    /**
     * @param Athletic $athletic
     * @return BikeSession[]|Collection
     */
    public function findByAthletic(Athletic $athletic): Collection;
}