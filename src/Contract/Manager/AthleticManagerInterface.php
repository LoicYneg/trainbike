<?php

namespace App\Contract\Manager;

use App\Entity\Athletic;
use App\ViewModel\StatsViewModel;

/**
 * Class AthleticManagerInterface
 * @package App\Manager
 */
interface AthleticManagerInterface
{
    /**
     * @param Athletic $athletic
     * @param bool $flush
     */
    public function createOrUpdate(Athletic $athletic, bool $flush = true): void;

    /**
     * @param Athletic $athletic
     * @param bool $flush
     */
    public function remove(Athletic $athletic, bool $flush = true): void;

    /**
     * @param array $keyAndValue
     * @return Athletic|null
     */
    public function findOneBy(array $keyAndValue): ?Athletic;

    /**
     * @param Athletic $athletic
     * @return StatsViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAthleticStatistics(Athletic $athletic): StatsViewModel;
}
