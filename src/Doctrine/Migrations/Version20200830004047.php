<?php

declare(strict_types=1);

namespace App\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200830004047 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE constants_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE constant_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE constant (id INT NOT NULL, athletic_id INT NOT NULL, bike_session_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, value NUMERIC(5, 2) NOT NULL, "uuid" UUID NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CB6AD5D85822567 ON constant ("uuid")');
        $this->addSql('CREATE INDEX IDX_CB6AD5D880C36D8 ON constant (athletic_id)');
        $this->addSql('CREATE INDEX IDX_CB6AD5D82FD85452 ON constant (bike_session_id)');
        $this->addSql('COMMENT ON COLUMN constant.type IS \'(DC2Type:enum_constant_type)\'');
        $this->addSql('COMMENT ON COLUMN constant."uuid" IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN constant.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN constant.updated_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('ALTER TABLE constant ADD CONSTRAINT FK_CB6AD5D880C36D8 FOREIGN KEY (athletic_id) REFERENCES athletic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE constant ADD CONSTRAINT FK_CB6AD5D82FD85452 FOREIGN KEY (bike_session_id) REFERENCES bike_session (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE constants');
        $this->addSql('ALTER TABLE bike_session ALTER energy TYPE INT');
        $this->addSql('ALTER TABLE bike_session ALTER energy DROP DEFAULT');
        $this->addSql('ALTER TABLE bike_session DROP COLUMN duration');
        $this->addSql('ALTER TABLE bike_session ADD duration TIMESTAMP(0) WITH TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE bike_session ALTER duration DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE constant_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE constants_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE constants (id INT NOT NULL, athletic_id INT NOT NULL, bike_session_id INT DEFAULT NULL, puls INT DEFAULT NULL, weight INT DEFAULT NULL, glycemia NUMERIC(5, 2) DEFAULT NULL, uuid UUID NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_93cdff385822567 ON constants (uuid)');
        $this->addSql('CREATE INDEX idx_93cdff382fd85452 ON constants (bike_session_id)');
        $this->addSql('CREATE INDEX idx_93cdff3880c36d8 ON constants (athletic_id)');
        $this->addSql('COMMENT ON COLUMN constants.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN constants.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN constants.updated_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('ALTER TABLE constants ADD CONSTRAINT fk_93cdff3880c36d8 FOREIGN KEY (athletic_id) REFERENCES athletic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE constants ADD CONSTRAINT fk_93cdff382fd85452 FOREIGN KEY (bike_session_id) REFERENCES bike_session (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE constant');
        $this->addSql('ALTER TABLE bike_session ALTER duration TYPE NUMERIC(5, 2)');
        $this->addSql('ALTER TABLE bike_session ALTER duration DROP DEFAULT');
        $this->addSql('ALTER TABLE bike_session ALTER duration DROP NOT NULL');
        $this->addSql('ALTER TABLE bike_session DROP duration');
        $this->addSql('ALTER TABLE bike_session ADD duration NUMERIC(5, 2)');
        $this->addSql('ALTER TABLE bike_session ALTER duration DROP DEFAULT');
    }
}
