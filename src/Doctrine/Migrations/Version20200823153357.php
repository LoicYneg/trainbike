<?php

declare(strict_types=1);

namespace App\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200823153357 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE athletic_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bike_session_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE constants_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE athletic (id INT NOT NULL, user_id INT NOT NULL, pseudo VARCHAR(255) DEFAULT NULL, "uuid" UUID NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E6A5BBBE5822567 ON athletic ("uuid")');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E6A5BBBEA76ED395 ON athletic (user_id)');
        $this->addSql('COMMENT ON COLUMN athletic."uuid" IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN athletic.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN athletic.updated_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE bike_session (id INT NOT NULL, athletic_id INT NOT NULL, distance NUMERIC(5, 2) DEFAULT NULL, duration NUMERIC(5, 2) NOT NULL, energy NUMERIC(5, 2) DEFAULT NULL, "uuid" UUID NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6AF84CE95822567 ON bike_session ("uuid")');
        $this->addSql('CREATE INDEX IDX_6AF84CE980C36D8 ON bike_session (athletic_id)');
        $this->addSql('COMMENT ON COLUMN bike_session."uuid" IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN bike_session.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN bike_session.updated_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE constants (id INT NOT NULL, athletic_id INT NOT NULL, bike_session_id INT DEFAULT NULL, puls INT DEFAULT NULL, weight INT DEFAULT NULL, glycemia NUMERIC(5, 2) DEFAULT NULL, "uuid" UUID NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_93CDFF385822567 ON constants ("uuid")');
        $this->addSql('CREATE INDEX IDX_93CDFF3880C36D8 ON constants (athletic_id)');
        $this->addSql('CREATE INDEX IDX_93CDFF382FD85452 ON constants (bike_session_id)');
        $this->addSql('COMMENT ON COLUMN constants."uuid" IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN constants.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN constants.updated_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('ALTER TABLE athletic ADD CONSTRAINT FK_E6A5BBBEA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bike_session ADD CONSTRAINT FK_6AF84CE980C36D8 FOREIGN KEY (athletic_id) REFERENCES athletic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE constants ADD CONSTRAINT FK_93CDFF3880C36D8 FOREIGN KEY (athletic_id) REFERENCES athletic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE constants ADD CONSTRAINT FK_93CDFF382FD85452 FOREIGN KEY (bike_session_id) REFERENCES bike_session (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" DROP token');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE bike_session DROP CONSTRAINT FK_6AF84CE980C36D8');
        $this->addSql('ALTER TABLE constants DROP CONSTRAINT FK_93CDFF3880C36D8');
        $this->addSql('ALTER TABLE constants DROP CONSTRAINT FK_93CDFF382FD85452');
        $this->addSql('DROP SEQUENCE athletic_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bike_session_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE constants_id_seq CASCADE');
        $this->addSql('DROP TABLE athletic');
        $this->addSql('DROP TABLE bike_session');
        $this->addSql('DROP TABLE constants');
        $this->addSql('ALTER TABLE "user" ADD token VARCHAR(255) NOT NULL');
    }
}
