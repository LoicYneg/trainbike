<?php

declare(strict_types=1);

namespace App\Doctrine\Type;

use App\Enum\UserRoleEnum;

/**
 * Class UserRoleEnumType
 * @package App\Doctrine\Type
 */
final class UserRoleEnumType extends AbstractEnumType
{
    /**
     * @return string
     */
    protected function getEnumClassName(): string
    {
        return UserRoleEnum::class;
    }

    /**
     * @return string
     */
    protected function getSqlType(): string
    {
        return self::STRING;
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     */
    public function getName()
    {
        return 'enum_user_role';
    }
}