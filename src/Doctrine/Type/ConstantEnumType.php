<?php

declare(strict_types=1);

namespace App\Doctrine\Type;

use App\Enum\ConstantEnum;

/**
 * Class ConstantsEnumType
 * @package App\Doctrine\Type
 */
final class ConstantEnumType extends AbstractEnumType
{
    /**
     * @return string
     */
    protected function getEnumClassName(): string
    {
        return ConstantEnum::class;
    }

    /**
     * @return string
     */
    protected function getSqlType(): string
    {
        return self::STRING;
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     */
    public function getName()
    {
        return 'enum_constant_type';
    }
}