<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Athletic;
use App\Entity\User;
use Doudou\BaseBundle\Controller\AbstractBaseController;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Translation\TranslatorInterface;
use Throwable;

/**
 * Class AbstractBaseController
 * @package App\Controller
 */
class AbstractController extends AbstractBaseController
{

    /**
     * @return Athletic
     */
    public function getAthletic(): Athletic
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null === $user) {
            throw new UnauthorizedHttpException("You don't have the right to be there");
        }
        $athletic = $user->getAthletic();
        if (null === $athletic) {
            throw new UnauthorizedHttpException("You don't have the right to be there");
        }
        return $athletic;
    }
}
