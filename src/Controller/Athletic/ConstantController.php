<?php

declare(strict_types=1);

namespace App\Controller\Athletic;

use App\Contract\Manager\ConstantManagerInterface;
use App\Controller\AbstractController;
use App\Entity\Constant;
use App\Form\ConstantType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package App\Controller
 */
final class ConstantController extends AbstractController
{
    public function addAction(
        Request $request,
        ConstantManagerInterface $constantManager
    ): Response {
        $constant = new Constant();
        $constant->setTimes();
        $constantForm = $this->createForm(ConstantType::class, $constant, array(
            'action' => $this->generateUrl('constant_add'),
            'method' => 'POST'
        ));

        $constant->setTimes();
        $constantForm->handleRequest($request);
        if ($constantForm->isSubmitted() && $constantForm->isValid()) {
            $constant->setAthletic($this->getAthletic());
            $constantManager->createOrUpdate($constant);
            $this->addSuccessMessage($constant->getType()->getValue() . 'enregistrée avec succès, courage champion !');
            return $this->redirectToRoute('athletic_index');
        }
        return $this->render('constant/add.html.twig', array(
            'constant_form' => $constantForm->createView()
        ));
    }
}
