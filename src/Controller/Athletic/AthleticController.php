<?php

declare(strict_types=1);

namespace App\Controller\Athletic;

use App\Contract\Manager\AthleticManagerInterface;
use App\Controller\AbstractController;
use App\Entity\Athletic;
use App\Form\AthleticType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AthleticController
 * @package App\Controller
 */
class AthleticController extends AbstractController
{
    /**
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function newAction(
        Request $request,
        AthleticManagerInterface $athleticManager
    ): Response {
        $athletic = new Athletic();
        $form = $this->createForm(AthleticType::class, $athletic);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $athleticManager->createOrUpdate($athletic);
            $this->addSuccessMessage('Utilisateur créé avec succès');
            return $this->redirectToRoute('app_login');
        }
        return $this->render('athletic/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param AthleticManagerInterface $athleticManager
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function statsAction(
        AthleticManagerInterface $athleticManager
    ): Response {
        $athletic = $this->getAthletic();
        $bikeSessions = $athletic->getBikeSessions();
        $km = array();
        foreach ($bikeSessions as $bikeSession) {
            $km[] = $bikeSession->getDistance();
        }
        return $this->render('statistic/index.html.twig', array(
            'athletic' => $athletic,
            'km' => json_encode($km),
            'stats' => $athleticManager->getAthleticStatistics($athletic)
        ));
    }
}
