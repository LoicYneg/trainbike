<?php

declare(strict_types=1);

namespace App\Controller\Athletic;

use App\Contract\Manager\BikeSessionManagerInterface;
use App\Controller\AbstractController;
use App\Entity\BikeSession;
use App\Entity\Constant;
use App\Form\BikeSessionType;
use DateTimeImmutable;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BikeSessionController
 * @package App\Controller
 */
final class BikeSessionController extends AbstractController
{

    /**
     * @return Response
     */
    public function indexAction(): Response
    {
        $bikeSessions = $this->getAthletic()->getBikeSessions();
        return $this->render('bike_session/index.html.twig', array(
            'bike_sessions' => $bikeSessions
        ));
    }

    /**
     * @param Request $request
     * @param BikeSessionManagerInterface $bikeSessionManager
     * @return JsonResponse
     */
    public function searchAction(
        Request $request,
        BikeSessionManagerInterface $bikeSessionManager
    ): JsonResponse {
        $bikeSessions = $bikeSessionManager->findByAthletic($this->getAthletic());
        $results = array();
        foreach ($bikeSessions as $bikeSession) {
            $results[] = array(
                'id' => $bikeSession->getId(),
                'text' => 'Session du ' . $bikeSession->getCreatedAt()->format('d-M-Y')
                );
        }
        return new JsonResponse($results);
    }

    /**
     * @param Request $request
     * @param BikeSessionManagerInterface $bikeSessionManager
     * @return Response
     */
    public function addAction(
        Request $request,
        BikeSessionManagerInterface $bikeSessionManager
    ): Response {
        $constant = new Constant();
        $bikeSession = (new BikeSession())->setCreatedAt(new DateTimeImmutable());
        $bikeSession->setAthletic($this->getAthletic());
        $form = $this->createForm(BikeSessionType::class, $bikeSession, array(
            'action' => $this->generateUrl('bike_session_add'),
            'method' => 'POST'
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $constant->setAthletic($this->getAthletic());
            $bikeSession->setAthletic($this->getAthletic())
                ->setUpdatedAt($bikeSession->getCreatedAt());
            $bikeSessionManager->createOrUpdate($bikeSession);
            $this->addSuccessMessage('Nouvelle Session Enregistrée avec succès, Bravo champion !');
            return $this->redirectToRoute('athletic_index');
        }
        return $this->render('bike_session/add.html.twig', array(
           'form' => $form->createView()
        ));
    }
}
