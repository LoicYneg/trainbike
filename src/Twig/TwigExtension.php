<?php

declare(strict_types=1);

namespace App\Twig;

use DateTime;
use DateTimeInterface;
use Exception;
use App\Contract\Manager\PaginatorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * TwigExtension constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(
        TranslatorInterface $translator
    ) {
        $this->translator = $translator;
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return array(
        );
    }
}
