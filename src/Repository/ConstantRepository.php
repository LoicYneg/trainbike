<?php

namespace App\Repository;

use App\Entity\Constant;
use Doudou\BaseBundle\Repository\AbstractRepository;

/**
 * @method Constant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Constant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Constant[]    findAll()
 * @method Constant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConstantRepository extends AbstractRepository
{
}
