<?php

namespace App\Repository;

use App\Entity\AbstractPerson;
use Doudou\BaseBundle\Repository\AbstractRepository;

/**
 * @method AbstractPerson|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractPerson|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractPerson[]    findAll()
 * @method AbstractPerson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AbstractPersonRepository extends AbstractRepository
{
}
