<?php

namespace App\Repository;

use App\Entity\Athletic;
use Doudou\BaseBundle\Repository\AbstractRepository;

/**
 * @method Athletic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Athletic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Athletic[]    findAll()
 * @method Athletic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AthleticRepository extends AbstractRepository
{
    public const ATHLETIC_ALIAS = 'ATHLETIC_ALIAS';
    public const BIKE_SESSION = 'BIKE_SESSION';
    public const CONSTANT = 'CONSTANT';
}
