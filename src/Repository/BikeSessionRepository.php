<?php

namespace App\Repository;

use App\Entity\BikeSession;
use Doudou\BaseBundle\Repository\AbstractRepository;

/**
 * @method BikeSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method BikeSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method BikeSession[]    findAll()
 * @method BikeSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BikeSessionRepository extends AbstractRepository
{

}
