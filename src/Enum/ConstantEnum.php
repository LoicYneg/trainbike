<?php

namespace App\Enum;

use Doudou\BaseBundle\Enum\AbstractBaseEnum;

/**
 * Class ConstantsEnum
 * @package App\Enum
 *
 * @method static ConstantEnum WEIGHT()
 * @method static ConstantEnum PULS()
 * @method static ConstantEnum SATURATION()
 * @method static ConstantEnum TEMPERATURE()
 * @method static ConstantEnum GLYCEMIA()
 */
class ConstantEnum extends AbstractBaseEnum
{
    public const WEIGHT = 'WEIGHT';
    public const PULS = 'PULS';
    public const SATURATION = 'SATURATION';
    public const TEMPERATURE = 'TEMPERATURE';
    public const GLYCEMIA = 'GLYCEMIA';
}