<?php

namespace App\Enum;

use Doudou\BaseBundle\Enum\AbstractBaseEnum;
use Zul3s\EnumPhp\Enum;

/**
 * Class UserRoleEnum
 * @package App\Enum
 *
 * @method static UserRoleEnum ROLE_USER()
 * @method static UserRoleEnum ROLE_ADMIN()
 * @method static UserRoleEnum ROLE_SUPER_ADMIN()
 */
class UserRoleEnum extends AbstractBaseEnum
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
}
