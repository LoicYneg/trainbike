<?php

declare(strict_types=1);

namespace App\Dto;

use App\Entity\Athletic;
use DateTime;

/**
 * Class BikeSessionDto
 * @package App\Dto
 */
final class BikeSessionDto
{

    /**
     * @var float;
     */
    private $distance;
    /**
     * @var float;
     */
    private $duration;
    /**
     * @var float|null;
     */
    private $energy;
    /**
     * @var integer|null;
     */
    private $puls;
    /**
     * @var float|null;
     */
    private $weight;
    /**
     * @var float|null;
     */
    private $glycemia;
    /**
     * @var DateTime
     */
    private $createdAt;
    /**
     * @var Athletic
     */
    private $athletic;

    public function __construct()
    {
        $this->createdAt = new DateTime('Now');
    }

    /**
     * @return float|null
     */
    public function getDistance(): ?float
    {
        return $this->distance;
    }

    /**
     * @param float $distance
     */
    public function setDistance(float $distance): void
    {
        $this->distance = $distance;
    }

    /**
     * @return float|null
     */
    public function getDuration(): ?float
    {
        return $this->duration;
    }

    /**
     * @param float $duration
     */
    public function setDuration(float $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return float|null
     */
    public function getEnergy(): ?float
    {
        return $this->energy;
    }

    /**
     * @param float|null $energy
     */
    public function setEnergy(?float $energy): void
    {
        $this->energy = $energy;
    }

    /**
     * @return int|null
     */
    public function getPuls(): ?int
    {
        return $this->puls;
    }

    /**
     * @param int|null $puls
     */
    public function setPuls(?int $puls): void
    {
        $this->puls = $puls;
    }

    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @param float|null $weight
     */
    public function setWeight(?float $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return float|null
     */
    public function getGlycemia(): ?float
    {
        return $this->glycemia;
    }

    /**
     * @param float|null $glycemia
     */
    public function setGlycemia(?float $glycemia): void
    {
        $this->glycemia = $glycemia;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Athletic
     */
    public function getAthletic(): Athletic
    {
        return $this->athletic;
    }

    /**
     * @param Athletic $athletic
     */
    public function setAthletic(Athletic $athletic): void
    {
        $this->athletic = $athletic;
    }
}
